#!/usr/bin/env python3
#----------------------------------------------------------------------------
# Program to initialize archive disks
#----------------------------------------------------------------------------
import argparse
import os
import sys
import time
import subprocess

import colorama
from colorama import Fore, Style

# Path to disk file to modify
_PATH_TO_DISK = "/dev/disk/by-path/pci-0000:00:14.0-usb-0:4:1.0-scsi-0:0:0:0"

# Number of runs to put on the disk
_NUM_RUNS_PER_DISK = 29

def main():
    """ Entry point """
    colorama.init(autoreset=True)

    args = _parse_args()

    print(Fore.RED + Style.BRIGHT + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(Fore.RED + Style.BRIGHT + "!                       WARNING                      !")
    print(Fore.RED + Style.BRIGHT + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(Fore.RED + Style.BRIGHT + "This program will irreoverably erase the disk accessible at " )
    print(Fore.WHITE + Style.BRIGHT + _PATH_TO_DISK)
    print("")



    print(Fore.RED + Style.BRIGHT + "If you are sure you want to do this, type " + Fore.WHITE + "YES" + Fore.RED + " and push enter")
    print(">", end='')
    text = sys.stdin.readline()

    if text != 'YES\n':
        print("Quiting")
        return

    print("OK, continuing")

    print("Writing partition table onto drive")
    _setup_partition_table()

    print("Zero superblocks in ext2 run partitions")
    _clear_run_partition_data()

    print("Writing filesystem and ID file onto ID partition")
    _setup_id_partition(args.id)


def _setup_partition_table():
    """
    Writes the partition table onto the drive
    """

    # Constants for math
    SECTOR_SIZE = 512
    KiB = 1024
    MiB = KiB * 1024
    GiB = MiB * 1024

    cmd = "label: gpt\n"
    cmd += ' 1: start=2048, size=1MiB, name="ID", type=EBD0A0A2-B9E5-4433-87C0-68B6B72699C7\n'

    SECTORS_PER_RUN = int(64 * GiB / SECTOR_SIZE)

    for run_num in range(_NUM_RUNS_PER_DISK):
        partition_num = 2+run_num
        cmd += '{0:>2}: size={1}, name=Run{2:02}\n'.format(
            partition_num,
            SECTORS_PER_RUN,
            run_num)

    subprocess.run(
        ['sfdisk', _PATH_TO_DISK],
        input=bytes(cmd, encoding='ascii'),
        check=True)

    print("Sleeping 5s to let the writes finish")
    time.sleep(5)



def _setup_id_partition(drive_id):
    """
    Write the ID partition

    @param[in] drive_id   ID number for the drive
    """

    partition_name = _PATH_TO_DISK + "-part1"
    MOUNTPOINT = "./archive_drive_setup_mountpoint"

    subprocess.run(
        [ 'mkfs.fat', partition_name ],
        check=True)

    if not os.path.isdir(MOUNTPOINT):
        os.mkdir(MOUNTPOINT)

    subprocess.run(
        ['mount', partition_name, MOUNTPOINT],
        check=True)

    with open(os.path.join(MOUNTPOINT, 'ID.txt'), 'wt') as id_file:
        id_file.write(
            "SeptiScan Archive Drive\n" +
            "CytoVale Inc\n" +
            "\n" +
            "Drive ID: " + drive_id + "\n")


    subprocess.run(
        ['umount', '-l', MOUNTPOINT],
        check=True)

def _clear_run_partition_data():
    """
    Go through the run partitions and zero bytes 0-2047.
    This will zero the ext2 superblock
    """

    for run_num in range(_NUM_RUNS_PER_DISK):
        partition_num = 2+run_num

        with open(_PATH_TO_DISK + "-part{0}".format(partition_num), "wb") as part_data:
            part_data.write(b'\x00' * 2048)

            os.fsync(part_data.fileno())


def _parse_args():
    """ Parse arguments """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--id",
        type=str,
        help="ID string to include in the drive ID file",
        required=True)

    return parser.parse_args()

if __name__ == "__main__":
    main()
